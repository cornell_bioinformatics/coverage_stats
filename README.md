# coverage_stats.pl

This PERL script is developed for the PanAnd project (https://www.panand.org/) to evaluate Illumina sequencing reads coverage by alignment to a reference genome. The sorghum reference genome is used for the PANAND project.    

## Getting Started


### Prerequisites
1. bwa (must in the PATH) https://github.com/lh3/bwa  
2. samtools (must in the PATH) http://www.htslib.org/
3. PICARD (modify script, set value for $PICARD_PATH) https://broadinstitute.github.io/picard/
4. bedtools https://bedtools.readthedocs.io/en/latest/
5.  A tab-delimited text file "genome" with two columns: chromosome name and length (modify script, set value for $GENOME,  the genome file in the repository is for Sorghum_bicolor_NCBIv3)
6. A bed file for all CDS coordinates "CDS.merged.bed" (modify script, set value for $BED, the BED file is for Sorghum_bicolor_NCBIv3)
7. BAM files for each sample (sorted and samtools indexed)

### Installation
```
git clone https://bitbucket.org/cornell_bioinformatics/coverage_stats.git  
```

### Usage 
```
coverage_stats.pl PATH_TO_SORTED_INDEXED_BAM sampleName outDir tmpDir
```




### Output file
  * a tab-delimited text file with one row. The output directory is specified in the run argument, and output file name is the sampleName. The columns are: Sample #reads #GB #%mapped %dup GCov>0	GCov>1	GCov>5	CDSCov>0	CDSCov>1	CDSCov>5	NM=0	NM<=5	NM<=10	NM<=15.


## Authors
* **Qi Sun**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments


