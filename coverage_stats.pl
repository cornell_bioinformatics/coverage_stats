#!/usr/bin/perl
use strict;
use warnings;

#this version works with whole genome instead of chr1
#input must be sorted indexed bam file

my $PICARD_PATH="/programs/picard-tools-2.19.2/picard.jar";
my $BED="CDS.merged.bed";
my $GENOME="genome";

my $BAM=$ARGV[0];
my $SAMPLE=$ARGV[1];
my $OUTFILE = $ARGV[2];
my $tmpdir = $ARGV[3];

my $threads = 4;  #threads to run picard
#my $memoryPerThread = "2G";

unless (defined $tmpdir) {
	$tmpdir = "/workdir" ;
}


## command to generate CDS.merged.bed
#awk -v OFS='\t' '{if ($3=="CDS") print $1,$4-1,$5}' Sorghum_bicolor.Sorghum_bicolor_NCBIv3.44.chromosome.1.gff3 |sort -k2,2n > chr.CDS
#bedtools merge -i chr.CDSc > CDS.merged.bed


## get bam file for chr1
#system("samtools view -H $BAM  > $tmpdir/$SAMPLE.chr1.sam");
#system("samtools view $BAM  | awk '{if (\$3==\"1\") print}' >> $tmpdir/$SAMPLE.chr1.sam");
#system("samtools sort -T $tmpdir -\@ $threads -m $memoryPerThread -O bam -o $tmpdir/$SAMPLE.chr1.sorted.bam $tmpdir/$SAMPLE.chr1.sam");
#system("samtools index $tmpdir/$SAMPLE.chr1.sorted.bam");


#samtools stat
my $samtoolstat = `samtools flagstat $BAM `;
my @flagstats = proc_flagstat($samtoolstat);

## following operation on chr1 only
#$BAM = "$tmpdir/$SAMPLE.chr1.sorted.bam";

##dedup java -XX:ParallelGCThreads=2
system ("java -XX:ParallelGCThreads=$threads  -jar $PICARD_PATH EstimateLibraryComplexity I=$BAM O=$tmpdir/$SAMPLE.dedup_metrics.txt QUIET=true VERBOSITY=ERROR");
my $dup_pct = dupstat("$tmpdir/$SAMPLE.dedup_metrics.txt");
$dup_pct = sprintf("%.3f", $dup_pct); 

## get genome coverage
my $genome_hist = `genomeCoverageBed -ibam $BAM -max 100  |grep -P '^genome'  | head -n 10 `;
my @genome_coverage= proc_cov($genome_hist);


## get coverage in CDS region
my $cds_hist = `coverageBed -sorted -a $BED -b $BAM -hist -g $GENOME |grep  ^all | head -n 10 `;
my @cds_coverage = proc_cov($cds_hist);

## get coverage in 
my $nm= `bedtools intersect -sorted -abam $BAM -b $BED -wa -f 1 -g $GENOME | samtools view -|cut -f 12 |grep ^NM:i: | sed "s/NM:i://" | sort -n | uniq -c `;
my @nms_all = proc_nm($nm);


#my $nm = ""; open IN, "AN18NSCR000010.NM"; while (<IN>) {$nm .=$_;}
#my $nmq60 = `samtools view -b -q 5 $BAM| bedtools intersect -sorted -abam stdin -b $BED -wa -f 1 -g sb.genome | samtools view - |cut -f 15 |grep ^NM: | sed "s/NM:i://" | sort -n | uniq -c `;
#my $nmq60 = ""; open IN, "AN18NSCR000010.mapq10.NM"; while (<IN>) {$nmq60 .=$_;}
#my @nms_q60 = proc_nm($nmq60);

## output header
#Sample #reads #GB #%mapped %dup GCov>0	GCov>1	GCov>5	CDSCov>0	CDSCov>1	CDSCov>5	NM=0	NM<=5	NM<=10	NM<=15
## get alignment stat

open OUT, ">$OUTFILE";
print OUT (join "\t", ($SAMPLE, @flagstats, $dup_pct, @genome_coverage, @cds_coverage, @nms_all));
print OUT "\n";
close OUT;

## clean tmp vile
system ("rm $tmpdir/$SAMPLE.dedup_metrics.txt");


sub proc_cov
{
	my $line = shift;
	my @report_cov = (0, 1, 5);
	my $accutotal = 0; 
	my %accumulate = ();
	my @covs = ();
	foreach (split(/\n/, $line)) 
	{
		chomp;
		my @data = split /\s+/;
		my ($dep, $pct) = @data[1, 4]; 
	    $accutotal += $pct; 
		$accumulate{$dep} = $accutotal;		
	}


	for (my $i=0; $i<=$#report_cov; $i++) 
	{
		my $reportcov = $report_cov[$i];
		if (exists $accumulate{$reportcov}) 
		{
			$covs[$i] = sprintf("%.3f", (1-$accumulate{$reportcov}) );
		}
		else
		{
			$covs[$i] = 0;
		}
	}
	
	return @covs;
}

sub proc_nm
{
	my $nmline = shift;
	my @report_nm = (0, 5, 10, 15);
	my $total = 0;
	my %nm2count = ();
	my @nms = ();
	foreach (split(/\n/, $nmline)) 
	{
		if (/(\d+)\s+(\d+)/)
		{
			my ($count, $nm) = ($1, $2);
			$total += $count;
			$nm2count{$nm} = $total;
		}
	}

	if ($total>0) 
	{
		for (my $i=0; $i<=$#report_nm; $i++) 
		{
			my $reportnm = $report_nm[$i];
			if (exists $nm2count{$reportnm}) 
			{
				$nms[$i] = sprintf("%.3f", $nm2count{$reportnm}/$total);
			}
			else
			{
				$nms[$i] = 0;
			}
		}
	}
	return @nms;
}


sub proc_flagstat
{
	my $line = shift;
	my $total = 0;
	my $pctmapped = 0;

	foreach (split(/\n/, $line)) 
	{
		if (/^(\d+).+in total/) 
		{
			$total = $1;
		}
		elsif (/mapped \(([0-9.]+)%/)  
		{
			$pctmapped = $1;
		}	
	}
	my $gb = sprintf("%.1f", $total * 150/1e9);
	$pctmapped = sprintf("%.3f", $pctmapped/100); 
	return ($total, $gb, $pctmapped);
}

sub dupstat
{
	my $file = shift;
	open IN, $file;
	duploop:while (<IN>) 
	{
		last duploop if (/^LIBRARY/);
	}
	my $line = <IN>;
	close IN;
	my @data = split "\t", $line;
	my ($sample, $pct) = @data[0, 8];
	return $pct;
}